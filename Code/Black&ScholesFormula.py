from math import sqrt, log, exp, erf
import random
from numpy import arange
import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np
S0 = 100.0  # S0 = Stock price
strikes = [i for i in range(50, 150)]  # Exercise prices range
T = 1  # T = Time to expiration
r = 0.01  # r = risk-free interest rate
q = 0.02  # q = dividend yield
vol = 0.2  # vol = volatility
Nsteps = 100  # Number or steps in MC

# d1 = (log(S0 / strikes) + (r - q + (vol ** 2 / 2)) * T) / vol * sqrt(T)
# d2 = (log(S0 / strikes) + (r - q - (vol ** 2 / 2)) * T) / vol * sqrt(T)
# Nd1 = d1.cumsum()
# Nd2 = d2.cumsum()


# def BandS(a, b, c, d, e):
#    formula = a * b - c * d * e
#    return formula


# Price = BandS(S0, Nd1, strikes, exp(-r * T), Nd2)
mylist = []
for k in strikes:
    d1 = (log(S0/k)+(r-q+(vol**2/2)))/(vol*sqrt(T))
    d2 = (log(S0/k)+(r-q-(vol**2/2)))/(vol*sqrt(T))

    BaS = (S0*erf(d1))-(k*exp(-r*T)*erf(d2))
    mylist.append(BaS)



plt.plot(strikes, mylist)
plt.xlabel('Strike Price')
plt.ylabel('Option Price')
plt.title('Black-Scholes analytical 2')
plt.savefig(os.path.abspath('../Results/B&S_analytical_for_loop.png'))
plt.show()


MyBandS = pd.DataFrame({'STRIKE PRICE': strikes,
                        })
MyBandS['d1'] = ((log(S0) - np.log((MyBandS['STRIKE PRICE']))) + (r - q + (vol ** 2 / 2)) * T) / (vol * sqrt(T))

MyBandS['d2'] = ((log(S0) - np.log((MyBandS['STRIKE PRICE']))) + (r - q - (vol ** 2 / 2)) * T) / (vol * sqrt(T))


MyBandS['Price'] = (S0*(MyBandS['Nd1'])) - (MyBandS['STRIKE PRICE']*exp(-r*T)*MyBandS['Nd2'])

print(MyBandS)

plt.plot(MyBandS['STRIKE PRICE'],MyBandS['Price'])
plt.xlabel('Strike Price')
plt.ylabel('Option Price')
plt.title('Black-Scholes analytical')
plt.savefig(os.path.abspath('../Results/B&S_analytical.png'))
plt.show()
